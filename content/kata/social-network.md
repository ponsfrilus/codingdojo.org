---
title: "Social Network"
draft: false
date: "2022-04-07"
---

## About

A very large subject to learn the exemple mapping workshop.

Idea is to build a very light social network.

## Epics

* Posting: Thomas can publish messages a message.
* Reading: Alice can view all Thomas’s messages.
* Following: Charlie can subscribe to Thomas’s and Alice’s messages, and view an aggregated list of all subscriptions.
* Mentions: Alice can mention Charlie in a message using “@” like "@Charlie"
* Links: Thomas can share a link to a message
* Direct Messages: Alice can send a private message to Thomas

